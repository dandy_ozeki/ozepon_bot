class WelcomeController < ApplicationController
  require 'httpclient'
  # CSRF解除
  protect_from_forgery :except => [:callback]

  # facebook Webhooks用token
  VERIFY_TOKEN = ENV['VERIFY_TOKEN']

  ENDPOINT_URI = "https://graph.facebook.com/v2.6/me/messages?access_token=" + VERIFY_TOKEN
  # facebook Webhooksのcallbackアクション
  def subscribe
    # webhook 認証確認用
    if params["hub.verify_token"]
      if params["hub.verify_token"] == VERIFY_TOKEN
        render json: params["hub.challenge"]
      else
        render json: "Error token"
      end
    end
  end

  # メッセージコールバック用アクション
  def callback
    logger.info params

    # オウム返し
    message = params["entry"][0]["messaging"][0]

    if message.include?("message")
      text = message["message"]["text"]

      # botが返却するメッセージを選択
      case text
        # 挨拶
        when "はろー", "Hello", "hello", "おはよう", "こんにちは", "こんばんわ", "おやすみ"
          hello_message message
        when "smr"
          structured_message_receipt message
        when "smg"
          structured_message_generic message
        when "button"
          button message
        when "image"
          image_url message
        when "sample"
          sample_word message
        else
          # オウム返し
          parrot message
      end

    else
      logger.info 'botの発言です'
    end
    # レンダリングしない
    render :nothing => true
  end

  private

    # sampleに対応したの文字を返す
    def sample_word(message)
      sender_id = get_sender_id message
      text      = "smr,smg,button,imageの何かでサンプルがでるかも！？"

      logger.info "sender_id #{sender_id}"
      logger.info "text   #{text}"

      logger.info "ENDPOINT_URI #{ENDPOINT_URI}"
      request_content = {recipient: {id:sender_id},
                         message: {text: text}
      }

      # rest
      content_json = request_content.to_json
      call_http_client_post_json ENDPOINT_URI, content_json
    end

    # Image (url)
    def image_url(message)
      sender_id = get_sender_id message

      request_content = {
                          recipient:{
                              id:sender_id
                          },
                          message:{
                              attachment:{
                                  type:"image",
                                  payload:{
                                      url:"https://petersapparel.com/img/shirt.png"
                                  }
                              }
                          }
                      }
      # rest
      content_json = request_content.to_json
      call_http_client_post_json ENDPOINT_URI, content_json
    end

  # Structured Message - Button Template
    def button(message)
      sender_id = get_sender_id message

      request_content = {
                          recipient:{
                              id:sender_id
                          },
                          message:{
                              attachment:{
                                  type:"template",
                                  payload:{
                                      template_type:"button",
                                      text:"What do you want to do next?",
                                      buttons:[
                                          {
                                              type:"web_url",
                                              url:"https://petersapparel.parseapp.com",
                                              title:"Show Website"
                                          },
                                          {
                                              type:"postback",
                                              title:"Start Chatting",
                                              payload:"USER_DEFINED_PAYLOAD"
                                          }
                                      ]
                                  }
                              }
                          }
                      }
      # rest
      content_json = request_content.to_json
      call_http_client_post_json ENDPOINT_URI, content_json
    end
    # Structured Message - Generic Template
    def structured_message_generic(message)

      sender_id = get_sender_id message

      request_content = {
                          recipient:{
                              id:sender_id
                          },
                          message:{
                              attachment:{
                                  type:"template",
                                  payload:{
                                      template_type:"generic",
                                      elements:[
                                          {
                                              title:"Classic White T-Shirt",
                                              image_url:"http://petersapparel.parseapp.com/img/item100-thumb.png",
                                              subtitle:"Soft white cotton t-shirt is back in style",
                                              buttons:[
                                                  {
                                                      type:"web_url",
                                                      url:"https://petersapparel.parseapp.com/view_item?item_id=100",
                                                      title:"View Item"
                                                  },
                                                  {
                                                      type:"web_url",
                                                      url:"https://petersapparel.parseapp.com/buy_item?item_id=100",
                                                      title:"Buy Item"
                                                  },
                                                  {
                                                      type:"postback",
                                                      title:"Bookmark Item",
                                                      payload:"USER_DEFINED_PAYLOAD_FOR_ITEM100"
                                                  }
                                              ]
                                          },
                                          {
                                              title:"Classic Grey T-Shirt",
                                              image_url:"http://petersapparel.parseapp.com/img/item101-thumb.png",
                                              subtitle:"Soft gray cotton t-shirt is back in style",
                                              buttons:[
                                                  {
                                                      type:"web_url",
                                                      url:"https://petersapparel.parseapp.com/view_item?item_id=101",
                                                      title:"View Item"
                                                  },
                                                  {
                                                      type:"web_url",
                                                      url:"https://petersapparel.parseapp.com/buy_item?item_id=101",
                                                      title:"Buy Item"
                                                  },
                                                  {
                                                      type:"postback",
                                                      title:"Bookmark Item",
                                                      payload:"USER_DEFINED_PAYLOAD_FOR_ITEM101"
                                                  }
                                              ]
                                          }
                                      ]
                                  }
                              }
                          }
                      }
      # rest
      content_json = request_content.to_json
      call_http_client_post_json ENDPOINT_URI, content_json
    end

    # Structured Message Receipt Template
    def structured_message_receipt(message)
      sender_id = get_sender_id message

      request_content = {
                          recipient:{
                            id:sender_id
                          },
                          message:{
                            attachment:{
                              type:"template",
                              payload:{
                                template_type:"receipt",
                                recipient_name:"Stephane Crozatier",
                                order_number:"12345678902",
                                currency:"USD",
                                payment_method:"Visa 2345",
                                order_url:"http://petersapparel.parseapp.com/order?order_id=123456",
                                timestamp:"1428444852",
                                elements:[
                                  {
                                    title:"Classic White T-Shirt",
                                    subtitle:"100% Soft and Luxurious Cotton",
                                    quantity:2,
                                    price:50,
                                    currency:"USD",
                                    image_url:"http://petersapparel.parseapp.com/img/whiteshirt.png"
                                  },
                                  {
                                    title:"Classic Gray T-Shirt",
                                    subtitle:"100% Soft and Luxurious Cotton",
                                    quantity:1,
                                    price:25,
                                    currency:"USD",
                                    image_url:"http://petersapparel.parseapp.com/img/grayshirt.png"
                                  }
                                ],
                                address:{
                                  street_1:"1 Hacker Way",
                                  street_2:"",
                                  city:"Menlo Park",
                                  postal_code:"94025",
                                  state:"CA",
                                  country:"US"
                                },
                                summary:{
                                  subtotal:75.00,
                                  shipping_cost:4.95,
                                  total_tax:6.19,
                                  total_cost:56.14
                                },
                                adjustments:[
                                  {
                                    name:"New Customer Discount",
                                    amount:20
                                  },
                                  {
                                    name:"$10 Off Coupon",
                                    amount:10
                                  }
                                ]
                              }
                            }
                          }
                        }
      # rest
      content_json = request_content.to_json
      call_http_client_post_json ENDPOINT_URI, content_json
    end

    # facebookのユーザープロフィールを取得する
    def get_user_profile(user_id)

      endpoint_uri = "https://graph.facebook.com/v2.6/" + user_id.to_s + "?fields=first_name,last_name,profile_pic&access_token=" + VERIFY_TOKEN
      http_client = HTTPClient.new

      # rest
      profile = http_client.get endpoint_uri
      JSON.parse profile.body
    end

    # 引数のjsonをpostリクエストする
    def call_http_client_post_json(endpoint_uri, content_json)
      http_client = HTTPClient.new
      http_client.post(endpoint_uri, content_json, {
          'Content-Type' => 'application/json; charset=UTF-8'
      }){|responce, request, result, &block|
        logger.info "responce: #{responce}"
        logger.info "request:  #{request}"
        logger.info "result:   #{result}"
      }
    end

    # オウム返しアクション
    def parrot(message)
      sender_id = get_sender_id message
      text      = get_message_text message

      logger.info "sender_id #{sender_id}"
      logger.info "text   #{text}"

      logger.info "ENDPOINT_URI #{ENDPOINT_URI}"
      request_content = {recipient: {id:sender_id},
                         message: {text: text}
      }

      # rest
      content_json = request_content.to_json
      call_http_client_post_json ENDPOINT_URI, content_json
    end

  # 挨拶をする
  def hello_message(message)
    sender_id = get_sender_id message
    text      = get_message_text message

    # user_profileを取得
    profile = get_user_profile sender_id

    first_name = profile["first_name"]
    last_name  = profile["last_name"]

    logger.info "profile : #{profile}"
    logger.info "profile : #{first_name}"
    logger.info "profile : #{last_name}"

    message = "#{text}! #{first_name} "

    # message作成
    request_content = {recipient: {id:sender_id},
                       message: {text: message}
    }
    content_json = request_content.to_json

    # rest
    call_http_client_post_json ENDPOINT_URI, content_json
  end

  # senderのidを取得
  def get_sender_id(message)
    message["sender"]["id"]
  end

  # senderのメッセージを取得
  def get_message_text(message)
    message["message"]["text"]
  end
end
